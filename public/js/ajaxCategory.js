$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});

// add category
$(document).on('click', '.add-category', function () {
    // $('#addCategory').modal('show');
});

$('.modal-footer').on('click', '.add', function () {
    $.ajax({
        type: 'POST',
        url: '/admin/categories',
        data: {
            '_token': $('input[name=_token]').val(),
            'name': $('#name_add').val(),
            'price': $('#price_add').val(),
        },
        success: function (data) {
            alert('Add new category success');
            $('#categoryTable').append("<tr  id='category_" + data.category.id + "' class='item" + data.category.id + "'>"
                + "<td>" + data.category.id + "</td>"
                + "<td>" + data.category.name + "</td>"
                + "<td>" + data.category.price + "</td>"
                + "<td>"
                + "<button ​type='button' data-id='" + data.category.id + "' data-target='#editCategory' data-toggle='modal' class='edit-category btn btn-info btn-edit mr-1'>Edit</button>"
                + "<button type='button' data-id='" + data.category.id + "' data-target='#deleteCategory'data-toggle='modal' class='btn btn-danger delete-category'>Delete</button>"
                + "</td>"
                + "</tr>");

        },
    });
});

//delete category
$(document).on('click', '.delete-category', function () {
    let id = $(this).data('id');
    $('.delete').click(function () {
        $.ajax({
            url: '/admin/categories/' + id,
            dataType: 'json',
            type: 'delete',
            success: function (data) {
                toastr.success('Successfully delete category!', 'Success Alert', {timeOut: 500});
                $('#deleteCategory').modal('hide');
                $('#category_' + id).remove();
                // location.reload();
            },
        });
    });
});
