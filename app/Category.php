<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
    protected $fillable = [
        'id',
        'name',
        'price',
    ];

    public function addCategory($request)
    {
        $category = new Category();
        $category->id = $request->id;
        $category->name = $request->name;
        $category->price = $request->price;
        $category->save();

        return $category;
    }

    public function updateCategory($request, $id)
    {
        $category = Category::findOrFail($id);
        $category->id = $request->id;
        $category->name = $request->name;
        $category->price = $request->price;
        $category->save();
        return $category;

    }

    public function deleteCategory($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();

        return $category;
    }

}
