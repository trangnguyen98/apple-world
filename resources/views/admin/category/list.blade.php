@extends('admin.layout.master')

@section('title')
    Product category list
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="text-center">
                <h2>Category</h2>
            </div>

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif

            <div class="container">
                <button class="add-category btn btn-success" data-target="#addCategory" data-toggle="modal">
                    <span class="glyphicon glyphicon-eye-open"></span>Add category
                </button>

                <br><br>
                <div class="table-responsive">
                    <table class="table table-bordered" id="categoryTable">
                        <tr style="background-color:cadetblue">
                            <th style="color:white">ID</th>
                            <th style="color:white">Name</th>
                            <th style="color:white">Price</th>
                            <th style="color:white" width="280px">Action</th>
                        </tr>
                        @foreach ($categories as $category)
                            <tr id="category_{{ $category->id }}">
                                <td>{{ $category->id }}</td>
                                <td>{{ $category->name }}</td>
                                <td>{{ $category->price }}</td>
                                <td>
                                    <button data-url="#" ​type="button" data-id="{{ $category->id }}"
                                            data-target="#editCategory"
                                            data-toggle="modal" class="edit-category btn btn-info btn-edit">Edit
                                    </button>
                                    <button type="button" data-id="{{ $category->id }}" data-target="#deleteCategory"
                                            data-toggle="modal" class="btn btn-danger delete-category">Delete
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        @include('admin.category.add')
        @include('admin.category.delete')

        {!! $categories->links() !!}

        @endsection

        @section('script')
            <script src="{{ asset('js/ajaxCategory.js') }}"></script>
@endsection
