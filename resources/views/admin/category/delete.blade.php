<div id="deleteCategory" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="deletecategoryLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" role="form">
                @csrf
                <input type="hidden" name="delete_category">
                <div class="modal-header">
                    <h5 class="modal-title" id="modaldeletecategoryLabel">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure want to delete?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary " data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary delete">Delete</button>
                </div>
            </form>
        </div>
    </div>
</div>

