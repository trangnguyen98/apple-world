<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="csrf-token" content="{{csrf_token()}}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title')</title>

    <!-- Custom fonts for this template-->
    <link href="{{ asset('layout/backend/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('layout/backend/css2/font.css') }}" rel="stylesheet">
    <link href="{{ asset('layout/backend/css2/font-awesome.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('layout/backend/css2/sb-admin-2.min.css') }}" rel="stylesheet">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" rel="stylesheet">
    <script language="javascript" href="https://code.jquery.com/jquery-3.0.0.min.js"></script>

</head>

<body id="page-top">
<div id="wrapper">
    <!-- Menu -->
@include('admin.layout.menu')
<!-- End of Menu -->
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            <!-- Header -->
        @include('admin.layout.header')
        <!-- End of Header -->
            <div id="container-fluid">
                @yield('content')
            </div>
        </div>
    </div>
</div>
</body>

<!-- Bootstrap core JavaScript-->
<script src="{{ asset('layout/backend/js2/jquery.min.js') }}"></script>
<script src="{{ asset('layout/backend/js2/bootstrap.bundle.min.js') }}"></script>

<!-- Core plugin JavaScript-->
<script src="{{ asset('layout/backend/js2/jquery.easing.min.js') }}"></script>

<!-- Custom scripts for all pages-->
<script src="{{ asset('layout/backend/js2/sb-admin-2.min.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

@yield('script')

</html>

